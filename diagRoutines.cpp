//diagnostic variables from Mathematica notebook.
//This file is not relevant to run the code, but simplified bug-fixing because of a more natural way to pass arguments.
double two=2.017296577420296;
double three=3.705265010266263;
double eight=0.08585921795535444;
double pthree=0.3175638674630585;

double rhoDiag(double z, void *dump)
{
  spherical_vector q_pos, s_spin, n_spin;
  q_pos.carte_values(0.1,0.5,z);
  s_spin.spheri_values(1,two,three);
  n_spin.spheri_values(1,eight,pthree);
  double temp=0.0;
  temp = 1.0 + 2.0 * scalarProduct(q_pos,s_spin)/q_pos.mag() * scalarProduct(n_spin,q_pos)/q_pos.mag();
  temp-= scalarProduct(n_spin,s_spin);
  temp*= rhoL(q_pos.mag())/2.0;
  temp+= rhoU(q_pos.mag())/2.0*(1.0 + scalarProduct(n_spin,s_spin));
  return temp;
}

double integratorDiag(double lowerLimit, double upperLimit)
{
  gsl_function F;
  F.function = &rhoDiag;
  double result, error;
  gsl_integration_workspace * w = gsl_integration_workspace_alloc (10000);
  gsl_integration_qags (&F,lowerLimit,upperLimit,0,1e-10,10000,w,&result,&error);
  gsl_integration_workspace_free (w);
  return result;
}

//generates rhoU/L output
void outputRhoULDiag(string dbname)
{
  spherical_vector s_spin, n_spin;
  spherical_vector q_pos;
  s_spin.spheri_values(1,two,three);
  n_spin.spheri_values(1,eight,pthree);
  
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {

      for(double z=0.1; z<maxInterp;z+=0.1)
	    {


	      q_pos.carte_values(maxInterp,maxInterp,z);
	      
	      cout << q_pos.ycomp() << endl;
	      cout << q_pos.xcomp() << endl;
	      cout << q_pos.zcomp() << endl;
	      exporting << z << tab;
	      cout << "mag "<< q_pos.mag() << endl;
	      exporting << rho(q_pos,s_spin,n_spin) << tab;
	    }
      exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;
}


//generates d2data/brezel output
void outputResultsDiag(string dbname)
{
  spherical_vector q_pos,s_spin, n_spin;
  s_spin.spheri_values(1,two,three);
  n_spin.spheri_values(1,eight,pthree);

  warning(maxInterp);
	 
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      int i=0;
      for(double x=-maxInterp;x<=maxInterp;x+=stepsize[i])
	{
	  for(double y=-maxInterp;y<=maxInterp;y+=stepsize[i])
	    {
	      
	      q_pos.spheri_values(sqrt(x*x+y*y)+0.1,M_PI/2.0,atan2(y,x));
	      exporting << x << tab;
	      exporting << y << tab;
	      exporting << rho(q_pos,s_spin,n_spin)<< tab;
	      exporting << endl;
	    }
	  exporting << endl;
	}
       exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;
}

//this generates Ta2 outputs
void outputResultsDiag2(string dbname)
{
  spherical_vector q_pos,s_spin, n_spin;
  s_spin.spheri_values(1,two,three);
  n_spin.spheri_values(1,eight,pthree);

  double max=10;
  warning(max);
 
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      for(double z=0;z<max;z++)
	{
	  exporting << z << tab;
	  exporting << integrator(0.5,0.1,s_spin,n_spin,-z,z) << endl;
	}
      exporting.close();
    }

  else cout << "Unable to open file " << dbname << endl;
}

//this creates xt output
void outputResultsDiag3(string dbname)
{
  spherical_vector q_pos,s_spin, n_spin;
  s_spin.spheri_values(1,two,three);
  n_spin.spheri_values(1,pi-two,three-pi);

  double max=2.5;
  warning(max);	 
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      for(double x=-max;x<=max;x+=0.03)
	{
	  for(double y=-max;y<=max;y+=0.03)
	    {
	      exporting << x << tab;
	      exporting << y << tab;
	      exporting << integrator(x,y,s_spin,n_spin,-3,3) << endl;
	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;
}

void outputResults(string dbname, int number)
{
  //Initialises the two spins of the single nucleii
  spherical_vector q_pos,s_spin, n_spin;
  s_spin.random_spins0();
  n_spin.random_spins1();

  warning(maxInterp);

  dbname=dbname + "_" + to_string(number) + ".dat";
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      int i=0;
      for(int sx=0;sx<NUMT;sx++)
	{
	  for(int sy=0;sy<NUMT;sy++)
	    {
	      double xpos,ypos;
	      xpos = -maxInterp + sx * stepsize[i];
	      ypos = -maxInterp + sy * stepsize[i];
	      q_pos.spheri_values(sqrt(xpos*xpos+ypos*ypos),atan2(ypos,xpos),atan2(sqrt(xpos*xpos+ypos*ypos),0));
	      exporting << xpos << tab;
	      exporting << ypos << tab;
	      exporting << integrator(xpos,ypos,s_spin,n_spin,-8,8) << tab;
	      exporting << rho(q_pos,s_spin,n_spin) << tab;
	      exporting << endl;
	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;
}

//overlaying two protons
void outputSuperposition(string dbname, int number)
{
  //Initialises the four spins of the two nucleii
  spherical_vector q_pos1, s_spin1, n_spin1, q_pos2, s_spin2, n_spin2;
  s_spin1.random_spins0();
  n_spin1.random_spins1();
  s_spin2.random_spins1();
  n_spin2.random_spins0();

  warning(maxInterp);


  double limits=8.0;
  
  dbname=dbname + "_" + to_string(number) + ".dat";
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      int i=0;
      for(int sx=0;sx<NUMT;sx++)
	{
	  for(int sy=0;sy<NUMT;sy++)
	    {
	      double xpos1,xpos2,ypos,integ1,integ2;
	      xpos1 = -maxInterp + sx * stepsize[i];
	      xpos2 = -maxInterp + sx * stepsize[i] + B;
	      ypos = -maxInterp + sy * stepsize[i];

	      
	      integ1 = integrator(xpos1,ypos,s_spin1,n_spin1,-limits,limits);
	      if(xpos2 <= maxInterp)
		integ2 = integrator(xpos2,ypos,s_spin2,n_spin2,-limits,limits);
	      //cut-off for shifted nucleus; numerically reasonable since the density is quasi zero.
	      else if(xpos2 > maxInterp)
		integ2=0.0;
	      
	      q_pos1.spheri_values(sqrt(xpos1*xpos1+ypos*ypos),atan2(ypos,xpos1),atan2(sqrt(xpos1*xpos1+ypos*ypos),0));
	      q_pos2.spheri_values(sqrt(pow(xpos2,2)+ypos*ypos),atan2(ypos,xpos2),atan2(sqrt(pow(xpos2,2)+ypos*ypos),0));
	      
	      exporting << xpos1 << tab;
	      exporting << ypos << tab;
	      exporting << integ1 << tab;
	      exporting << integ2 << tab;
	      exporting << integ1+integ2 << tab;
	      exporting << rho(q_pos1,s_spin1,n_spin1) << tab;
	      exporting << rho(q_pos2,s_spin2,n_spin2) << tab;
	      exporting << xpos2 << tab;
	      
	      exporting << integ1+integ2 << tab;

	      exporting << endl;
	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;
}
