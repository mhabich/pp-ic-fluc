PWD = `pwd`

DEBUG =
OPTIMIZATION = -O3
FLOWTRACE =
PARALL = -fopenmp
STANDARD = -std=c++14
CFLAGS = $(DEBUG) $(OPTIMIZATION) $(FLOWTRACE) $(PARALL) $(STANDARD)
COMPILER = g++
LIBS = -lm -lgsl -lgslcblas 
INCLUDES = -I. -Igsl/include/


S10=\
protonshape.cpp

EXE10=\
protonshape


protonshape: 
	$(COMPILER) $(PARALL) $(S10) -o $(EXE10) $(LIBS) $(STANDARD) $(OPTIMIZATION)

clean: 
	rm -f $(OBJ10) $(OBJ11)
	rm -f $(EXE10) $(EXE11)
	rm -f results/*.png results/*.dat
	rm -f *_pbs.log
