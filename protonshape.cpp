#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_integration.h>
#include <omp.h>
#include <random>
#include <sstream>
#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>

using namespace std;

double *stepsize, **funca, **funcb, ****centrho, ****integrho, **integrho_norm;
int fileLength;

const string tab="\t";
const string prefix="===> ";
const double pi=M_PI;
const double fmtoGeV=5.0677;

const int events=1000;
const int NUMT=50;
int *numt;

//averaging over/vanishing spins
const bool spin_avg=false;
//old interpolation maximum, will be deprecated
const double maxInterp=5.0;
//radial interpolation maximum
const double interp_max=5.0;

//This is the maximum impact parameter determining the system size based on the interp_max from the rho_L and rho_U
//const double BMax=2.0;
//physical size maximum without having to create values beyond the interpolation region
//const double sizeMax=2.0*interp_max-B_max;


double sizeMax=4.0;
//number of lattices
const int lattices=1;
//scales the energy density rho for one nucleus; for two nucleii edScale*edScale (squared)
double edScale=1.0;


//overwritten in 'void evaluate()'
double B=0.0;
//maximum impact parameter
const double BMax=1.0;

//seed to generate random but identical IC for each run --- not event!
int seed=11111;

std::mt19937_64 rng( seed );
std::default_random_engine rng1( seed );
std::uniform_real_distribution<> dist(0, 2*M_PI);
std::uniform_real_distribution<> impactB(0, BMax);

//not used
void createDir(int impact, int resolution)
{
  string foldername;
  foldername = "mkdir results_" + to_string(impact) + "_" + to_string(resolution);
  int dump = system(foldername.c_str());
}

int lineNumbers(string dbname)
{
  ifstream import (dbname.c_str());
  string dump;
  int length=0;
  if (import.is_open())
  {
      while ( getline (import,dump) )
      {
	length++;
      }
    import.close();
  }
  else cout << "Unable to open count lines for file 'funca.dat'" << endl;
  //returns the file length; equivalent to 'wc -l'
  return length;
}

void allocateLattices()
{
  numt = new int[lattices];
  numt[0] = NUMT;
  //  numt[1] = numt[0]*2;
  //  numt[2] = numt[1]*2;

  for(int i=0;i<lattices;i++)
    stepsize = new double[lattices];

  cout << "Lattice points" << tab << "Step size [fm]" << tab << "AT [1/GeV]" << endl;
  for(int i=0;i<lattices;i++)
    {

      stepsize[i] = double(sizeMax/(numt[i]-1));
      cout << numt[i] << tab << tab << stepsize[i] << tab << stepsize[i]*fmtoGeV << endl;
    }
}

void allocateMemory()
{
  
  funca = new double*[2];
  for(int i=0;i<2;i++)
    funca[i] = new double[fileLength];
  funcb = new double*[2];
  for(int i=0;i<2;i++)
    funcb[i] = new double[fileLength];

  integrho = new double***[lattices];
  for(int i=0;i<lattices;i++)
    integrho[i] = new double**[2];
  for(int i=0;i<lattices;i++)
    for(int j=0;j<2;j++)
      integrho[i][j] = new double*[numt[i]];
  for(int i=0;i<lattices;i++)
    for(int j=0;j<2;j++)
      for(int k=0;k<numt[i];k++)
	integrho[i][j][k] = new double[numt[i]];

  integrho_norm = new double*[lattices];

  for(int i=0;i<lattices;i++)
    integrho_norm[i] = new double[2];
  
  centrho = new double***[lattices];
  for(int i=0;i<lattices;i++)
    centrho[i] = new double**[2];
  for(int i=0;i<lattices;i++)
    for(int j=0;j<2;j++)
      centrho[i][j] = new double*[numt[i]];
  for(int i=0;i<lattices;i++)
    for(int j=0;j<2;j++)
      for(int k=0;k<numt[i];k++)
	centrho[i][j][k] = new double[numt[i]];

  cout << prefix << "Memory allocated." << endl;
  
}

void importingFile(string dbname, double **array)
{
  ifstream importing (dbname.c_str());
  if (importing.is_open())
  {
    for(int i=0;i<lineNumbers(dbname.c_str());i++)
      {
	importing >> array[0][i];
	importing >> array[1][i];
      }
    importing.close();
  }
  else cout << "Unable to open file " << dbname << endl;
  cout << "File imported: " << tab << dbname << endl;
}

void warning(double critical)
{
  if(critical>5.6) 
    {
      cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << endl;
      cout << "WARNING: interpolation might break down." << endl;
      cout << "Quark's position is at the edge of numerically stable interpolation." << endl;
      cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << endl;
    }
}

class spherical_vector 
{ 
  double radius, phi, theta;
public:
    void carte_values(double x, double y, double z)
  {
    radius = sqrt(x*x+y*y+z*z);
    phi = atan2(y,x);
    theta = atan2(sqrt(x*x+y*y),z);
  }
  void spheri_values(double r, double th, double ph)
  {
    radius = r;
    theta = th;
    phi = ph;
  }
  void random_spins0()
  {
    radius = 1;
    phi = dist(rng);
    theta = dist(rng);
  }
  void random_spins1()
  {
    //different random number generator
    radius = 1;
    phi = dist(rng1);
    theta = dist(rng1);
  }
  double xcomp()
  {
    return radius*sin(theta)*cos(phi);
  }
  double ycomp()
  {
    return radius*sin(theta)*sin(phi);
  }
  double zcomp()
  {
    return radius*cos(theta);
  }
  double mag()
  {
    return radius;
  }
};

double scalarProduct(spherical_vector r1, spherical_vector r2)
{
  double temp;
  temp = r1.xcomp()*r2.xcomp() + r1.ycomp()*r2.ycomp() + r1.zcomp()*r2.zcomp();
  return temp;
}

double rhoL(double radius)
{
  double temp;
  gsl_interp_accel *acc = gsl_interp_accel_alloc ();
  gsl_spline *spline = gsl_spline_alloc (gsl_interp_cspline, fileLength);
  gsl_spline_init (spline, funcb[0], funcb[1], fileLength);
  //The scaling by 1/r^2 diverges at the centre; thus, interpolation breaks down
  temp = gsl_spline_eval (spline, radius, acc)/radius/radius;
  gsl_spline_free (spline);
  gsl_interp_accel_free (acc);
  return temp;
}

double rhoU(double radius)
{
  double temp;
  gsl_interp_accel *acc = gsl_interp_accel_alloc ();
  gsl_spline *spline = gsl_spline_alloc (gsl_interp_cspline, fileLength);
  gsl_spline_init (spline, funca[0], funca[1], fileLength);
  //The scaling by 1/r^2 diverges at the centre; thus, interpolation breaks down
  temp = gsl_spline_eval (spline, radius, acc)/radius/radius;
  gsl_spline_free (spline);
  gsl_interp_accel_free (acc);
  return temp;
}

struct rhoParameters 
{
  double x, y; 
  spherical_vector s_spin, n_spin;
};

double rho(spherical_vector q_pos, spherical_vector s_spin, spherical_vector n_spin)
{
  double temp = 0.0;
  temp = rhoL(q_pos.mag())/2.0 * (1.0 + 2.0 * scalarProduct(q_pos,s_spin)/q_pos.mag() * scalarProduct(n_spin,q_pos)/q_pos.mag() - scalarProduct(n_spin,s_spin)) + rhoU(q_pos.mag())/2.0*(1.0 + scalarProduct(n_spin,s_spin));
  temp*= edScale;
  return temp;
}

double rhoVarReducer(double z, void *p)
{
  struct rhoParameters * rhoParams =  (struct rhoParameters *)p;
  double x = (rhoParams->x);
  double y = (rhoParams->y);
  spherical_vector s_spin = (rhoParams->s_spin);
  spherical_vector n_spin = (rhoParams->n_spin);
  spherical_vector q_pos;
  q_pos.carte_values(x,y,z);
  return rho(q_pos, s_spin, n_spin);
}

//integrator for just the boost direction
double integrator(double x, double y, spherical_vector s_spin, spherical_vector n_spin, double lowerLimit, double upperLimit)
{
  gsl_function F;

  struct rhoParameters rhoParams = {x,y,s_spin,n_spin};
  F.function = &rhoVarReducer;
  F.params =  &rhoParams;

  double result, error;
  gsl_integration_workspace * w = gsl_integration_workspace_alloc (10000);
  gsl_integration_qags (&F,lowerLimit,upperLimit,0,1e-7,10000,w,&result,&error);
  gsl_integration_workspace_free (w);
  return result;
}

struct integrhoParameters
{
  spherical_vector s_spin, n_spin;
};

double rhoVarReducer_monte(double *r, size_t dim, void *params)
{
  struct integrhoParameters *integrhoParams = (struct integrhoParameters *)params;
  spherical_vector s_spin = (integrhoParams->s_spin);
  spherical_vector n_spin = (integrhoParams->n_spin);
  spherical_vector q_pos;
  q_pos.carte_values(r[0],r[1],r[2]);
  return rho(q_pos, s_spin, n_spin);
}

//Monte Carlo integrator over all three dimensions
double integrated_rho(spherical_vector s_spin, spherical_vector n_spin)
{
  double result, error;
  struct integrhoParameters integrhoParams = {s_spin,n_spin};
  double lim_lo[3] = { -sizeMax/2.0, -sizeMax/2.0, -sizeMax/2.0 };
  double lim_up[3] = { +sizeMax/2.0, +sizeMax/2.0, +sizeMax/2.0 };
  const gsl_rng_type *T;
  gsl_rng *r;
  //This points to the function, specifies the dimension, and points to the fct.'s parameters
  gsl_monte_function G = { &rhoVarReducer_monte,3,&integrhoParams};
  gsl_monte_plain_state *s = gsl_monte_plain_alloc (3);
  gsl_rng_env_setup();

  //Places the random seed?
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);

  //Number of iterations
  size_t calls = 1000000;
  
  gsl_monte_plain_integrate(&G, lim_lo, lim_up, 3, calls, r, s, &result, &error);
  gsl_monte_plain_free(s);
  return result;
}

#include "diagRoutines.cpp"

//npart scheme
double overlap(int sx, int sy, int lattice, double sigma)
{
  double T[2], temp;
  //converts sigma from [mb] to [fm^2]; note that 'rhoU' and 'rhoL' are in fm^{-2} (atomic units: m_e = 1)
  sigma/= 10.0;
  
  //initialises the values at each lattice point
  for(int nucleus=0;nucleus<2;nucleus++)
    {
      T[nucleus]  = integrho[lattice][nucleus][sx][sy]/integrho_norm[lattice][nucleus];
    }
  //calculates the overlap between each nucleus, smoothed by the exp fct.s
  temp = T[0]*(1-exp(-sigma * T[1])) + T[1]*(1-exp(-sigma * T[0]));
  return temp;
}


//main evaluation/calculation routine
void evaluate()
{
  spherical_vector s_spin0,n_spin0;
  spherical_vector s_spin1,n_spin1;
  double limits=interp_max;
  


  if(spin_avg==false)
    {
      //sets intitial spins for both protons
      s_spin0.random_spins0();
      n_spin0.random_spins1();
      s_spin1.random_spins1();
      n_spin1.random_spins0();
    }
  else if(spin_avg==true)
    {
      //spin-averaged protons
      s_spin0.carte_values(0,0,0);
      n_spin0.carte_values(0,0,0);
      s_spin1.carte_values(0,0,0);
      n_spin1.carte_values(0,0,0);
    }
  
  
  //This changes the impact parameter 'B' globally!
  B=impactB(rng);

  warning(maxInterp);

  
  for(int i=0;i<lattices;i++)
    {
#pragma omp parallel for num_threads(omp_get_max_threads())   
      for(int sx=0;sx<numt[i];sx++)
	{
	  for(int sy=0;sy<numt[i];sy++)
	    {
	      spherical_vector q_pos0,q_pos1;
	      double  xpos0,xpos1,ypos;

	      xpos0 = -sizeMax/2.0 + sx * stepsize[i] + B/2.0;
	      xpos1 = -sizeMax/2.0 + sx * stepsize[i] - B/2.0;
	      ypos = -sizeMax/2.0 + sy * stepsize[i];

	      if(abs(xpos0) > interp_max)
		cout << "Warning: xpos0 is out of stable interpolation/integration bounds" << endl;
	      if(abs(xpos1) > interp_max)
		cout << "Warning: xpos1 is out of stable interpolation/integration bounds" << endl;
	      
	      q_pos0.carte_values(xpos0,ypos,0);
	      q_pos1.carte_values(xpos1,ypos,0);

	      //centrho[i][0][sx][sy] = rho(q_pos0,s_spin0,n_spin0);
	      //centrho[i][1][sx][sy] = rho(q_pos1,s_spin1,n_spin1);
	  
	      integrho[i][0][sx][sy] = integrator(xpos0,ypos,s_spin0,n_spin0,-limits,limits);
	      integrho[i][1][sx][sy] = integrator(xpos1,ypos,s_spin1,n_spin1,-limits,limits);

	      //if(sy==3)
	      //cout << sx << tab << xpos0 << tab << xpos1 << endl;	      
	    }
	}
    }
  //Monte Carlo integration of the charge density to obtain the normalisation constant;
  //deviates slightly at the 5th decimal from Riemann sum; no idea about faster or slower than Riemann sum
  //  cout << "monte carlo0" << tab << integrated_rho(s_spin0,n_spin0) << endl;
  //cout << "monte carlo1" << tab << integrated_rho(s_spin1,n_spin1) << endl;

  
  //This integrates the charge density; no idea about faster or slower than MC
  for(int lattice=0;lattice<lattices;lattice++)
    {
      cout << "Normalisation of nucleii:" << tab;
      for(int nucleus=0;nucleus<2;nucleus++)
	{
	  //integrhoSummed(lattice, nucleus);
	  double temp=0;
          #pragma omp parallel for reduction (+:temp) num_threads(omp_get_max_threads())
	  for(int sx=0;sx<NUMT;sx++)
	    for(int sy=0;sy<NUMT;sy++)
	      temp+= integrho[lattice][nucleus][sx][sy];
	  temp*= stepsize[lattice] * stepsize[lattice];
	  integrho_norm[lattice][nucleus] = temp;
	  cout << temp << tab;
	}
      cout << endl;
    }
}




//This contains more information in a better format than the 'outputMatrixNpart' and 'outputMatrixNcoll' routines
void outputList(string dbname, int lattice, int number)
{
  warning(maxInterp);
  
  dbname=dbname + to_string(numt[lattice]) + "_" + to_string(number) + ".dat";
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      for(int sx=0;sx<numt[lattice];sx++)
	{
	  for(int sy=0;sy<numt[lattice];sy++)
	    {
	      double  xpos0,xpos1,ypos;
	      //Each lattice has a different stepsize
	      xpos0 = -sizeMax/2.0 + sx * stepsize[lattice];
	      xpos1 = -sizeMax/2.0 + sx * stepsize[lattice];
	      ypos = -sizeMax/2.0 + sy * stepsize[lattice];
	      
	      exporting << xpos0 << tab;
	      exporting << ypos << tab;
	      exporting << integrho[lattice][0][sx][sy] + integrho[lattice][1][sx][sy] << tab;
	      exporting << centrho[lattice][0][sx][sy] + centrho[lattice][1][sx][sy] << tab;
	      exporting << integrho[lattice][0][sx][sy] << tab;
	      exporting << integrho[lattice][1][sx][sy] << tab;
	      exporting << centrho[lattice][0][sx][sy] << tab;
	      exporting << centrho[lattice][1][sx][sy] << tab;
	      exporting << integrho[lattice][0][sx][sy] * integrho[lattice][1][sx][sy] << tab;
	      exporting << endl;
	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;
}

//Output for the added, integrated energy densities, normalised in 'overlap'
void outputMatrixNpart(string dbname, int lattice, int number)
{
  warning(maxInterp);
  if (lattices!=1)
    {
      cout << "Warning: your output files are overwritten for each lattice!" << endl;
      cout << "Add additional number to each output file in 'outputMatrix' and out!" << endl;
    }
  ostringstream leadingZeros;
  leadingZeros << setw(3) << setfill('0') << number;
  dbname=dbname + leadingZeros.str() + ".dat";
  //cross section in [mb]
  double sigma = 40.0; 
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      for(int sy=0;sy<numt[lattice];sy++)
	{
	  for(int sx=0;sx<numt[lattice];sx++)
	    {
	      exporting << overlap(sx,sy,lattice,sigma) << tab;
	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;

}

//Output for the multiplied, integrated energy densities, normalised
void outputMatrixNcoll(string dbname, int lattice, int number)
{
  warning(maxInterp);
  if (lattices!=1)
    cout << "Warning: your output files are overwritten for each lattice!" << endl;

  ostringstream leadingZeros;
  leadingZeros << setw(3) << setfill('0') << number;
  dbname=dbname + leadingZeros.str() + ".dat";

  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      for(int sy=0;sy<numt[lattice];sy++)
	{
	  for(int sx=0;sx<numt[lattice];sx++)
	    {
	      double  xpos0,xpos1,ypos;
	      exporting << integrho[lattice][0][sx][sy] * integrho[lattice][1][sx][sy] / integrho_norm[lattice][0] / integrho_norm[lattice][1] << tab;
	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;

}


void plotscript(string dbname, int number)
{
  ofstream exporting (dbname.c_str());
  if (exporting.is_open())
    {
      exporting << "set view map" << endl;
      exporting << "set size square" << endl;
      exporting << "set pm3d" << endl;
      exporting << "set contour base" << endl;
      exporting << "set cntrparam levels 10" << endl;
      exporting << "set term png" << endl;
      exporting << "column1=3" << endl;
      exporting << "column2=4" << endl;
      exporting << "input2='results/rho_" << NUMT << "_'" << endl;
      exporting << "input1='results/inited_pp_'" << endl;
      exporting << "output1='results/contour_'" << endl;
      exporting << "output2='results/alt_contour_'" << endl;
      for (int i=0;i<events;i++)
	{
	  exporting << "set out output1.'" << i << ".png'" << endl;
	  exporting << "plot input1.'" << setw(3) << setfill('0') << i << ".dat' matrix with image" << endl;
	  exporting << "set out output2.'" << i << ".png'" << endl;
	  exporting << "splot input2.'" << i << ".dat' u 1:2:column2 with pm3d" << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << dbname << endl;
}

//triggers all the outputs and calculations
void performOutputs()
{
  cout << prefix << "Performing outputs." << endl;
  int counter=0;

  for(int j=0;j<events;j++)
    {
      evaluate();
      for(int i=0;i<lattices;i++)
	{
	  //outputList("results/rho_",i,j);
	  outputMatrixNpart("results/npart/inited_pp_",i,j);
	  outputMatrixNcoll("results/ncoll/inited_pp_",i,j);
	  counter++;
	  if(double(counter%10)==0)
	    cout << "Outputs written: " << tab << counter << endl;
	}
    }

  plotscript("contours.plt",events);
  //cout << "==> Plotting results." << endl;
  //int dump = system("gnuplot 'contours.plt'");
  
}

void preamble()
{
  cout << endl;
  cout << prefix << "This is protonshape-1.9." << endl;
  cout << endl;
  cout << "Number of cores: " << tab << omp_get_max_threads() << endl;
  cout << "Number of events: " << tab << events << endl;
  cout << "System size [fm]: " << tab << sizeMax << endl;
  cout << "Scaling: " << tab << edScale << endl;
  cout << "BMax [fm]: " << tab << BMax << endl;
  cout << endl;
}

int main()
{
  preamble();
  allocateLattices();

  fileLength=lineNumbers("input/funca.dat");
  //Array size is determined by number of lines
  allocateMemory();

  importingFile("input/funca.dat",funca);
  importingFile("input/funcb.dat",funcb);

  performOutputs();
  
  cout << "==> Done." << endl;
  return 0;
}
